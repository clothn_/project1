# Shape Run #

### Table of Content
* [Source code](Project1_Code/)
* [Demo video](https://youtu.be/k6ghTFfi19U)
    * The video has some latency issues due to the limit of recording environment.
* Description of projects and notes in README.md (this file).


---


## Description of Shape Run
### What is Shape Run?
Shape Run is a simple run & jump game, controlling a rolling ball to avoid obstacles on its way (similar to Cookie Run or the Google dinosaur game).

The rules of Shape Run are:

* If the "jump" button is short pressed, the ball jumps shortly.
* If the "jump" button is long pressed, the ball jumps for a long time.
* The player starts with 5 lives, and whenever the ball hits an obstacle, the player loses one life. When no life is left, the game ends.
* When the ball hits an obstacle, the speed of the ball changes. The change of speed differs by the shape of the obstacle.
* Score increments while the ball doesn't hit any obstacle.


### Dashboard Settings
* In the dashboard, the player can customize the number of initial lives or the speed of the ball.
* Or, the player can use custom modes provided. The custom modes include:
    1. Easy
        - Starts with 5 lives
        - No speed change when hitting obstacles
    2. Normal
    3. Hard
        - Starts with 3 lives
        - Speed increases every 5 points
* In the dashboard, the player can also create new shapes of obstacles by drawing.

### Music Copyright
* BGM (no copyright): 샛별 - [Sugar Cookie](https://youtu.be/7lqpOlVYtD0)


---


## Code Structure
### Libraries Used
* Minim

### Classes Overview
* Ball
* Game
* JumpButton
* Background
* Obstacles
    * Square
    * Triangle
    * Circle

### Ball
The class Ball is for creating the jumping ball.

* Constructor

        Ball () {
            jumpState = false;
            y = 500;
            velocity = 0;
        }

* Draw
        
        void draw () {
            fill (BALL_COLOR);
            noStroke ();
            ellipseMode (CENTER);

            if (!jumpState) {
                ellipse (BALL_X, y, BALL_SIZE, BALL_SIZE);
            } else {
                ellipse (BALL_X, y - 200, BALL_SIZE, BALL_SIZE);
            }
        }

    Depending on whether the ball is jumping or not, the draw function draws the ball in its correct 'y' position.

* Detect Collision

        boolean detectCollision (Obstacles obstacle) {
            if (!jumpState) {
                int obstacleX = obstacle.getX ();
                if (BALL_X - BALL_SIZE/2 < obstacleX && obstacleX < BALL_X + BALL_SIZE/2) return true;
            }
            return false;
        }
    The detectCollision function receives an obstacle and detects whether the ball has collided with the obstacle. If there is a collision, the function returns the boolean 'true'.

* Is Jumping

        boolean isJumping () {
                return jumpState;
            }
    The isJumping function returns the boolean, telling whether the ball is jumping or not at the current state.

* Toggle

        void toggle (boolean jumpState) {
            this.jumpState = jumpState;
        }
    When the mouse clicks on the jump botton, the toggle function changes the jump state of the ball.


### Game
* Constructor

        Game () {
            imgs= new PImage [2];
            imgs[0]= loadImage ("Heart-01.png");
            imgs[1]= loadImage ("Heart-02.png");

            obstacles = new ArrayList <Obstacles> ();
            backgrounds = new ArrayList <Background> ();

            Background background1 = new Background (640);
            Background background2 = new Background (1920);
            backgrounds.add (background1);
            backgrounds.add (background2);

            int randomNumber = round (random (1.5, 4.5));
            switch (randomNumber) {
                case 2:
                    obstacles.add (new Circle (1280 + OBSTACLE_WIDTH/2, OBSTACLE_WIDTH));
                case 3:
                    obstacles.add (new Triangle (1280 + OBSTACLE_WIDTH/2, OBSTACLE_WIDTH, OBSTACLE_WIDTH));
                case 4:
                    obstacles.add (new Square (1280 + OBSTACLE_WIDTH/2, OBSTACLE_WIDTH, OBSTACLE_HEIGHT));
            }

            score = 0;
            highestScore = 0;
            lives = 5;
            speed = DEFAULT_SPEED;
            start = true;  
        }
    The game has several local variables including score, highest score, lives, speed and start. In the constructor, all these values are initialized and obstacles and backgrounds are added.

* Draw

    The draw function consists of many parts, so the explanations are divided for easy understanding.

        // background graphics
        for (Background background : backgrounds) {
            background.move (speed);
            if (background.getX () < - 640) {
                background.moveTo (1920);
            }
            background.draw ();
        }
    Add background graphics to the game.

        // game start
        if (start) {
            player.play ();
            // obstacles
            int obsNum = obstacles.size ();
            if (obstacles.get (obsNum - 1).getX () < 1150) {
                // int randomNumber = round (random (0.5, 4.5));
                int xPosition = 1280 + OBSTACLE_WIDTH;
                int randomIndex = round (random (0.5, 4.5));
                if (randomIndex == 1) {
                    for (int i = 0; i < 2; i++) {
                        int position = 1280 + (2*i + 1)*OBSTACLE_WIDTH/2;
                        obstacles.add (randomObstacleGenerator (1.5, 4.5, position));
                    }
                } else {
                    obstacles.add (randomObstacleGenerator (1.5, 4.5, xPosition));
                }
            }
    Randomly generate obstacles.

        // update obstacles
        for (int i = 0; i < obstacles.size (); i++) {
            Obstacles obstacle = obstacles.get (i);
            obstacle.move (speed);
            obstacle.draw ();
            
            if (ball.detectCollision (obstacle)) {
                lives -= 1;
                obstacles.remove (i);
                speed += obstacle.changeSpeed ();
                // println ("collided :" + obstacle.changeSpeed () + ", lives: " + lives);
            } else score += 1;

            if (obstacle.getX () < 0) obstacles.remove (i);
        }
    Update obstacles and detect collision for each.

        // text score
        fill (0);
        PFont font = loadFont("GentiumBasic-Bold-48.vlw");
        textFont (font, 24);
        text ("Score: " + score, 1120, 60);

        // hearts
        imageMode (CENTER);
        for (int i = 0; i < lives; i++) {
            int x = 50 * (i+1);
            int y = 50;
            image (imgs[1], x, y, 50, 50);
        }
    Graphic displays for the score and lives.

        // end game
        if (lives == 0) {
            // end game
            start = false;
            speed = 0;
            player.pause ();

            // save highest score
            if (highestScore < score) {
                highestScore = score;
            }

            // game over background
            rectMode (CENTER);
            fill (BACKGROUND_COLOR);
            noStroke ();
            rect (640, 360, 1280, 720);

            fill (FOREGROUND_COLOR);
            noStroke ();
            rect (640, 720, 1280, 360);

            // show 'game over' text
            fill (0);
            textAlign (CENTER);
            textFont (font, 48);
            text ("Game Over", 640, 280);

            // show 'final score' and 'highest score'
            textFont (font, 24);
            text ("Final Score: " + score, 640, 320);
            text ("Press Spacebar to restart.", 640, 400);
            text ("( Highest Score: " + highestScore + " )", 640, 350);
        }
    End the game when there are no lives left, and display the final score and the highest score.

* Reset Game

        void resetGame () {
            obstacles = new ArrayList <Obstacles> ();
            backgrounds = new ArrayList <Background> ();

            Background background1 = new Background (640);
            Background background2 = new Background (1920);
            backgrounds.add (background1);
            backgrounds.add (background2);

            obstacles.add (randomObstacleGenerator (1.5, 4.5, 1280 + OBSTACLE_WIDTH/2));

            score = 0;
            lives = 5;
            speed = DEFAULT_SPEED;
            start = true;
            player.rewind ();
        }
    The resetGame function resets all the variables, obstacles, backgrounds and the BGM.

### JumpButton
* Constructor

        JumpButton (int x, int y) {
            this.x = x;
            this.y = y;
            pressState = false;
        }

* Draw

        void draw () {
            rectMode (CENTER);
            if (pressState == false) {
                fill (BUTTON_COLOR_OFF);
            } else fill (BUTTON_COLOR_ON);
            noStroke ();
            rect (x, y, BUTTON_WIDTH, BUTTON_HEIGHT);

            fill (255);
            PFont font = loadFont("GentiumBasic-Bold-48.vlw");
            textAlign (CENTER);
            textFont (font, 24);
            text ("Jump", x, y + 6);
        }
    The draw function draws the jump button depending on its press state.

* Toggle

        void toggle (boolean pressState) {
            this.pressState = pressState;
        }
    The toggle button changes the press state of the jump button whenever the jump button is clicked.

* Is Over

        boolean isOver () {
            if (mouseX < x - BUTTON_WIDTH/2 || mouseX > x + BUTTON_WIDTH/2) return false;
            if (mouseY < y - BUTTON_HEIGHT/2 || mouseY > y + BUTTON_HEIGHT/2) return false;
            return true;
        }
    The isOver function is used to detect if the mouse is clicking on the correct area of the jump button. It returns a boolean value of whether the mouse is on the jump button area or not.


* Is Pressed

        boolean isPressed () {
            return pressState;
        }
    The isPressed function returns the press state of the jump button.

### Background
* Constructor

        Background (int x) {
            this.x = x;
            img = loadImage ("Background.png");
        }

* Move

        void move (float speed) {
            x -= speed;
        }
    The move function updates the 'x' position of the background image by speed.

* Draw

        void draw () {
            image (img, x, 144, 1160, 288);
        }
    The draw function draws the background image at the set 'x' position.

* Move To

        void moveTo (int position) {
            x = position;
        }
    The moveTo function moves the 'x' position of the background image to the input position.

* Get X

        int getX () {
            return x;
        }
    The getX function returns the 'x' position of the background image.


### Obstacles
The class Obstacles has child classes that inherit it, such as class Square, Triangle and Circle.

* Constructor

        Obstacles (int x) {
            this.x = x;
        }

* Draw

        // blank method for overriding
        void draw () {} 
    The draw function draws the obstacle in its position.

* Move

        void move (float speed) {
            x -= speed;
        }
    The move function updates the 'x' position of the obstacle by speed.

* Change Speed

        float changeSpeed () {
            return 0;
        }
    The changeSpeed function returns the designated speed for each obstacles: 3 for square, 2 for triangle and 1 for circle.

* Get X

        int getX () {
            return x;
        }
    The getX function returns the 'x' position of the obstacle.
	
### Random Obstacle Generator

This is a function, not a class.

    // generate random obstacles
    Obstacles randomObstacleGenerator (float min, float max, int x) {
        Obstacles obstacle = null;
        int randomNumber = round (random (min, max));
        switch (randomNumber) {
            case 2:
                obstacle = new Circle (x, OBSTACLE_WIDTH);
                break;
            case 3:
                obstacle = new Triangle (x, OBSTACLE_WIDTH, OBSTACLE_HEIGHT);
                break;
            case 4:
                obstacle = new Square (x, OBSTACLE_WIDTH, OBSTACLE_HEIGHT);
                break;
        }
        return obstacle;
    }

This function randomly generates an obstacle on the given 'x' position.









