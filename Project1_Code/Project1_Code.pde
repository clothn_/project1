/*
	Project 1
	Name of Project: Shape Run
	Author: Haena Cho
	Date: 20.05.15 ~ 20.05.30
*/

// CONSTANTS
public final color BACKGROUND_COLOR = color (247, 232, 246);
// color FOREGROUND_COLOR = color (241, 198, 231);
public final color FOREGROUND_COLOR = color (229, 176, 234);
public final int BUTTON_WIDTH = 150;
public final int BUTTON_HEIGHT = 70;
public final int BUTTON_COLOR_OFF = color (189, 131, 206);
public final int BUTTON_COLOR_ON = color (160, 110, 180);

public int pressTime;
public int releaseTime;

Minim minim;
AudioPlayer player;

Game game;
JumpButton button;
Ball ball;

void setup()
{
	size (1280, 720);
	game = new Game ();
	button = new JumpButton (1140, 630);
	ball = new Ball ();
    minim = new Minim(this);
    player = minim.loadFile("SugarCookie.mp3");
	println (STUDENT_ID);
}

void draw()
{
	// game background
	rectMode (CENTER);
	fill (BACKGROUND_COLOR);
	noStroke ();
	rect (640, 360, 1280, 720);

	// game foreground
	fill (FOREGROUND_COLOR);
	noStroke ();
	rect (640, 720, 1280, 360);

	game.draw ();
	button.draw ();
    ball.draw ();

	if (releaseTime == 0) {
		if (millis() - pressTime > 800) {
			button.toggle (false);
			ball.toggle (false);
		}
	} else {
		if (releaseTime - pressTime < 400) {
			if (millis () - pressTime > 400) {
				button.toggle (false);
				ball.toggle (false);
			}
		} else {
			if (millis () - pressTime > 800) {
				button.toggle (false);
				ball.toggle (false);
			}
		}
	}
}

void mousePressed () {	
	if (button.isOver () && !button.isPressed ()) {
        pressTime = millis ();
		button.toggle (true);
		ball.toggle (true);
        releaseTime = 0;
	}
}

void mouseReleased () {
	releaseTime = millis ();
}

void keyPressed () {
	if (key == ' ') game.resetGame ();
}

class Ball {
    private boolean jumpState;
    private int y;

    Ball () {
        jumpState = false;
        y = 500;
    }

    void draw () {
        fill (BALL_COLOR);
        noStroke ();
        ellipseMode (CENTER);

        if (!jumpState) {
            ellipse (BALL_X, y, BALL_SIZE, BALL_SIZE);
        } else {
            ellipse (BALL_X, y - 200, BALL_SIZE, BALL_SIZE);
        }
    }

    boolean detectCollision (Obstacles obstacle) {
        if (!jumpState) {
            int obstacleX = obstacle.getX ();
            if (BALL_X - BALL_SIZE/2 < obstacleX && obstacleX < BALL_X + BALL_SIZE/2) return true;
        }
        return false;
    }

    boolean isJumping () {
        return jumpState;
    }

    void toggle (boolean jumpState) {
        this.jumpState = jumpState;
    }
}